var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var cors = require('cors')

var dummyUsers = [
  {
    username: 'Józsi',
    numberOfTransactions: 9876,
    isProfessional: false,
  },
  {
    username: 'Karcsi',
    numberOfTransactions: 123,
    isProfessional: true,
  },
  {
    username: 'Pisti',
    numberOfTransactions: 54,
    isProfessional: false,
  }
];

var dummyUserIndex = 0;

app.use(cors())

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

setInterval(() => {
  var user = dummyUsers[(dummyUserIndex++) % dummyUsers.length];
  io.emit('bidder-updated', user);
}, 2000);

io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
